# 前言
根据jts工具,结合实际使用场景提取出来的常用工具集合,涵盖几何转换(WKT,GeoJSON等)、gis距离计算、角度计算、buffer运算、映射截取、几何穿串等操作
# gis-tools使用说明
## 1. 格式转换
### 1.1 WktTool使用说明
wkt格式与geometry互转;
1. wkt转geometry操作
```
Geometry geometry = WktTool.wktToGeo("LINESTRING (116.7262994 35.5153623, 116.726309 35.515362, 116.7263135 35.5153642, 116.72632 35.515364, 116.72633 35.515363, 116.7263355 35.5153617, 116.726343 35.515362)");
```
2. geometry转wkt操作
```
//转换不带Z
WktTool.geoToWkt2D(geometry);
//转换带Z
WktTool.geoToWkt3D(geometry);
```
### 1.2 GeoJsonTool使用说明
1. geoJson字符串转Geometry
> GeoJsonTool.read(String geoJson)
2. Geometry转GeoJson 
> GeoJsonTool.toJSONString(Geometry geometry)
## 2. 扩圈工具
### 2.1 BufferTool使用说明
将geometry进行扩圈处理,线场景使用最广,支持延线的方向左右扩、单边扩;
1. 圆角扩圈(上下是矩形，左右是圆角)
```
//distance单位米
BufferTool.bufferCapRound(Geometry geometry, double distance)
```
![示例图片](docs/statics/buffercapround.png)

2. 扩圈(上下左右四个方向,外接多边形)
```
//distance单位米
BufferTool.bufferCapSquare(Geometry geometry, double distance)
```
3. 上下扩圈(扩完是矩形)
```
//distance单位米
BufferTool.bufferCapFat(Geometry geometry, double distance)
```
4. 左扩X米右扩X米
```
//distance单位米 leftDistance 左扩x米 rightDistance 右扩X米
BufferTool.bufferGeoLeftRight(Geometry geometry, double leftDistance, double rightDistance)
```
5. 左扩X米
```
//distance单位米 distance 左扩x米
BufferTool.bufferGeoLeft(Geometry geometry, double distance)
```
![示例图片](docs/statics/leftxbuffer.png)
6. 右扩X米
```
//distance单位米 distance 右扩x米
BufferTool.bufferGeoRight(Geometry geometry, double distance)
```
## 3. 距离工具
### 3.1 DistanceTool使用说明
点到点距离计算,线距离计算,点到线距离计算,度距离相关转换等工具类,半径采用地球平均半径6371.0087714;
1. 距离转换为度 cmToDegree
2. 距离(KM)转换为度 kmToDegree
3. 度转换为距离(单位KM) degreeToKm
4. 度转换为距离(单位M) degreeToM
5. 度转换为厘米 degreeToCm
6. 获取几何距离单位(米) getLengthM(Geometry geometry)
7. 获取几何距离单位(千米) getLengthKM(Geometry geometry)
8. 获取两点的距离 p2pLenDegrees(Coordinate coordinate0, Coordinate coordinate1)
9. 获取两点距离(KM) p2pLenKm(Coordinate coordinate0, Coordinate coordinate1)
10. 获取两点距离(M) p2pLenM(Coordinate coordinate0, Coordinate coordinate1)
11. 点到线的最近距离 p2LineDegrees(Point point, LineString lineString)
12. 点到线的最近距离(KM) p2LineKM(Point point, LineString lineString)
13. 点到线的最近距离(M) p2LineM(Point point, LineString lineString)
14. 通过turf计算距离(椭球计算) distanceByTurf(LineString lineString, String unit)
## 4. 角度工具
### 4.1 AngleTool使用说明
1. 获取顺时针夹角 getCwAngle(Coordinate p0, Coordinate p1, Coordinate p2)
> p1: po,p2的交点坐标
2. 获取 逆时针夹角 getCCwAngle(Coordinate p0, Coordinate p1, Coordinate p2)
> p1: po,p2的交点坐标
3. 获取两条线段的逆时针夹角 getCCwAngle(Geometry lineString0, Geometry lineString1)
4. 获取两条线段的顺时针夹角 getCwAngle(Geometry lineString0, Geometry lineString1)
5. 判断点coordinate在线lineString的左边还是右边 isLeft(Geometry lineString, Coordinate coordinate)
6. 判断whereLineString在lineString的左侧还是右侧 isLeft(Geometry lineString, Geometry whereLineString)
## 5. 操作工具
### 5.1 PointTool使用说明
1. 通过线设置point的Z值 getPointZ(Geometry linkString, Point point)
2. 获取平均Z getAvgZ(Geometry geometry)
### 5.2 LineTool使用说明
1. 获取点到线的垂线 verticalLine(Point point, Geometry lineString)
2. 获取垂足(点到线的垂足) (Point point, Geometry lineString)
3. 截取l1在点coordinate0与coordinate1之间的线段 (Geometry l1, Coordinate coordinate0, Coordinate coordinate1)
4. 获取l2在l1上面的线段 getExtractLine(Geometry l1, Geometry l2)
5. 获取l2在l1上面的线段，并借用l1距离垂足xm的形状点，没有的话返回垂足做起、终点 getExtractLine(Geometry l1, Geometry l2, double xm)
6. 获取point在l1上面的位置点(索引位置) PointLocation getLinearLocation(Geometry l1, Geometry point)
> PointLocation index:索引位置 segmentFraction:同一个index下面的占比. 应用场景：点到线做映射,然后排序映射后的点
7. 获取 pointGeoList 在线 l1上面的投影线 getExtractLineByPoints(Geometry l1, List<Geometry> pointGeoList)
8. 当geometry两两形状点的跨度大于XM的时候往geometry里面加入形状点 sewing(Geometry geometry, int XM)
9. 根据距离计算线上点的位置 ST_InterpolatePoint(Geometry geometry, double distance)
10. 通过一根线上面的线段信息,通过等比例关系去截取另一个线上面的信息 LineLabelTool.lineLabelToGeometry(Geometry targetGeometry, Geometry sourceGeometry, List<LineLabel> lineLabelList)
# gis-tools扩展工具使用说明
## 1. skewer穿串工具
**使用场景通过数据的挂接开始、结束node关系,完成例如道路线的穿串**
### 1.1.1 创建操作类继承Seeker接口
```
findNode: 寻找下个节点; stopCondition 停止条件
```
> 例如: LinkSeeker
### 1.1.2 使用者调用
```
LinkSeeker rdLinkSeeker = new LinkSeeker();
Skewer<Link> skewer = new Skewer<>(rdLinkSeeker, SeekerWay.POINT_END, link);
List<LinkedList<Node<Link>>> linkedLists = skewer.searchKebab(node,LinkSeeker.linkData);
```
**使用场景通过数据的point起终形状点,完成例如道路线的穿串**
### 1.2.1 创建操作类继承SeekerPoint接口
```
findNode: 寻找下个节点; stopCondition 停止条件
```
> 例如: LinkSeekerByPoint
### 1.2.2 使用者调用
```
LinkSeekerByPoint rdLinkSeeker = new LinkSeekerByPoint();
SkewerPoint<Link> skewer = new SkewerPoint<>(rdLinkSeeker, SeekerWay.POINT_END, link);
List<LinkedList<Node<Link>>> linkedLists = skewer.searchKebab(node, LinkSeekerByPoint.linkData);
```

## 2. shadow投影计算工具
使用场景,例如: 输入数据: 道路线要素,路牙要素、护栏要素; 通过路牙、护栏要素在道路线上面制作新的要素A; 路牙与护栏共存输出A1,只有路牙A2,只有护栏A3
### 2.1 构造输入对象
```
List<LineInput> lineInputList = new ArrayList<>();
 
//构造 路牙 护栏对象
LineInput lineInput = new LineInput();
lineInput.setLineString(几何);
lineInput.setPartType(路牙); 
lineInput.setNeedShadow(true);
lineInputList.add(lineInput);

lineInputList.add(lineInput);
```
### 2.2 计算
```
LineLabelFactory lineLabelFactory = new LineLabelFactory(lineInputList, 道路线要素几何);
List<LineLabel> lineLabelList = lineLabelFactory.getLineLabel(partTypeSet->{
    if(partTypeSet.contains(1)){
        return 1;
    }else if(partTypeSet.contains(2)){
        return 2;
    }else {
        return PartType.EMPTY;
    }
});
```
## 3.地理坐标系Geographic coordinate system
[地理坐标系wiki](https://en.wikipedia.org/wiki/Geographic_coordinate_system)
1. 地理坐标系统(GCS)是一种球面或大地坐标系，用于测量和直接通信地球上的位置，如纬度和经度。[1]它是目前使用的各种空间参考系统中最简单、最古老和最广泛使用的，并构成了大多数其他空间参考系统的基础。虽然纬度和经度形成了一个类似直角坐标系的坐标元组，但地理坐标系不是直角坐标系，因为测量值是角度，不在平面上。
完整的GCS规范，例如EPSG和ISO 19111标准中列出的规范，还包括大地基准(包括地球椭球)的选择，因为不同的基准将为同一位置产生不同的纬度和经度值。

2. 地理坐标系是使用三维球面来定义地球表面位置，以实现通过经纬度对地球表面点位引用的坐标系。一个地理坐标系包括角度测量单位、本初子午线和参考椭球体三部分。在球面系统中，水平线是等纬度线或纬线。垂直线是等经度线或经线。常见的 WGS 84、CGCS 2000 等都属于地理坐标系。[来自知乎](https://zhuanlan.zhihu.com/p/505738565) [来自知乎](https://luosgeo.com/)

## 3.投影转换
[地图投影概念wiki](https://en.wikipedia.org/wiki/Map_projection)
在制图学中，地图投影是用于在平面上表示球体的二维弯曲表面的一组广泛变换中的任何一种。在地图投影中，地球表面位置的坐标（通常表示为纬度和经度）被转换为平面坐标
### 3.1 方位等距投影
[方位等距投影wiki](https://en.wikipedia.org/wiki/Azimuthal_equidistant_projection)
1. 参考EquidistantAzimuthalProjectionTool
## 4. GeoHashTool工具
进行中......