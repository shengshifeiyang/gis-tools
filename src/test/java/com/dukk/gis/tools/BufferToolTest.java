package com.dukk.gis.tools;

import com.dukk.gis.tools.BufferTool;
import com.dukk.gis.tools.WktTool;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;

public class BufferToolTest {

    public static void main(String[] args) throws ParseException {
        Geometry line = WktTool.wktToGeo("LINESTRING (116.7262962 35.51535525, 116.7263001 35.51535535, 116.72630445 35.5153553, 116.726309 35.515355, 116.726314 35.515355, 116.7263176 35.5153549, 116.7263228 35.5153545, 116.72632685 35.5153545, 116.7263309 35.5153546)");

        //distance单位米
        Geometry capBuffer = BufferTool.bufferCapRound(line, 1);
        System.out.println("圆角扩圈(上下是矩形，左右是圆角):"+ capBuffer.toText());

        Geometry capSquare = BufferTool.bufferCapSquare(line, 1);
        System.out.println("扩圈(上下左右四个方向,外接多边形):"+ capSquare.toText());

        Geometry capFat = BufferTool.bufferCapRound(line, 1);
        System.out.println("上下扩圈(扩完是矩形):"+ capFat.toText());

        Geometry leftXBuffer = BufferTool.bufferGeoLeft(line, 1);
        System.out.println("左阔1米:"+leftXBuffer.toText());


    }
}
