package com.dukk.gis.tools;

import org.junit.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateList;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;

import java.util.List;

public class LineToolTest {

    @Test
    public void subLineUseCoordinatesTest() throws ParseException {
        Geometry lineString = WktTool.wktToGeo("LINESTRING (116.7262962 35.51535525, 116.7263001 35.51535535, 116.72630445 35.5153553, 116.726309 35.515355, 116.726314 35.515355, 116.7263176 35.5153549, 116.7263228 35.5153545, 116.72632685 35.5153545, 116.7263309 35.5153546)");

//        MULTIPOINT ((116.7263023 35.5153554), (116.7263114 35.515355), (116.7263203 35.5153547))
        CoordinateList coordinates = new CoordinateList();
        coordinates.add(new Coordinate(116.7263023, 35.5153554));
        coordinates.add(new Coordinate(116.7263114, 35.515355));
        coordinates.add(new Coordinate(116.7263203, 35.5153547));

        List<Geometry> geometryList = LineTool.subLineUseCoordinates(lineString, coordinates.toCoordinateArray());
        for (Geometry geometry : geometryList) {
            System.out.println(geometry.toText());
        }
    }

}
