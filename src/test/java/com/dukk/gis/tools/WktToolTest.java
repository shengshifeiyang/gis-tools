package com.dukk.gis.tools;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;

public class WktToolTest {

    public static void main(String[] args) throws ParseException {
        Geometry geometry = WktTool.wktToGeo("LINESTRING (116.7262994 35.5153623, 116.726309 35.515362, 116.7263135 35.5153642, 116.72632 35.515364, 116.72633 35.515363, 116.7263355 35.5153617, 116.726343 35.515362)");

        System.out.println(WktTool.geoToWkt2D(geometry));
        System.out.println(WktTool.geoToWkt3D(geometry));
    }
}
