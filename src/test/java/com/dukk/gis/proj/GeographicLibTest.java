package com.dukk.gis.proj;

import net.sf.geographiclib.Geodesic;
import net.sf.geographiclib.GeodesicData;

/**
 * https://github.com/geographiclib/geographiclib-java
 *
 * 方位角等距投影 计算距离类库
 */
public class GeographicLibTest {

    public static void main(String[] args) {

        GeodesicData geodesicData = Geodesic.WGS84.Inverse(23.5295,111.1775,23.514,111.1775);
        System.out.println(geodesicData.lat1);
        System.out.println(geodesicData.lon1);
        System.out.println(geodesicData.a12);
        System.out.println(geodesicData.m12);
        System.out.println(geodesicData.azi1);
        System.out.println(geodesicData.azi2);
        System.out.println(geodesicData.s12); //距离



    }
}
