package com.dukk.gis.proj;

import org.locationtech.proj4j.*;
import org.locationtech.proj4j.proj.EquidistantAzimuthalProjection;

public class ProjTransformingCoorTest {

    public static void main(String[] args) {
        CRSFactory crsFactory = new CRSFactory();
        CoordinateReferenceSystem WGS84 = crsFactory.createFromName("epsg:4326");
        CoordinateReferenceSystem UTM = crsFactory.createFromName("epsg:25833");


        CoordinateTransformFactory ctFactory = new CoordinateTransformFactory();
        CoordinateTransform wgsToUtm = ctFactory.createTransform(WGS84, UTM);

        // `result` is an output parameter to `transform()`
        ProjCoordinate result = new ProjCoordinate();
      //  wgsToUtm.transform(new ProjCoordinate(lon, lat), result);

        ProjCoordinate srcPro = new ProjCoordinate();
        srcPro.setValue(109.333,22.8535);

        ProjCoordinate result2 = new ProjCoordinate();
        EquidistantAzimuthalProjection equidistantAzimuthalProjection = new EquidistantAzimuthalProjection();
        ProjCoordinate p = equidistantAzimuthalProjection.project(srcPro, result2);


        System.out.println(result2.x);

        ProjCoordinate result3 = new ProjCoordinate();
        ProjCoordinate p2 = equidistantAzimuthalProjection.inverseProject(p, result3);


        System.out.println(result3.x);


    }
}
