package com.dukk.gis.proj;

import com.dukk.gis.tools.BufferTool;
import com.dukk.gis.tools.DistanceTool;
import com.dukk.gis.tools.GeoTool;
import com.dukk.gis.tools.WktTool;
import com.jhlabs.map.proj.*;
import org.junit.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateList;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.io.ParseException;
import org.locationtech.spatial4j.distance.DistanceUtils;

import java.awt.geom.Point2D;

public class LambertAzimuthalEqualAreaProjectionTest {


    @Test
    public void name() {


    }

    public static void main(String[] args) throws ParseException {

        Geometry wtkGeo = WktTool.wktToGeo("LINESTRING (126.75421695 48.07578329, 126.75420559 48.07573163, 126.75415943 48.07552171, 126.75415747 48.07551282,126.75413987 48.07543274, 126.75412813 48.07537936, 126.75412618 48.07537046, 126.75411323 48.07531111111)");

//        LambertAzimuthalEqualAreaProjection p = new LambertAzimuthalEqualAreaProjection();
        EquidistantAzimuthalProjection p = new EquidistantAzimuthalProjection();
//        EqualEarthProjection p = new EqualEarthProjection();


        CoordinateList coordinateProject = new CoordinateList();

        Coordinate[] coordinates = wtkGeo.getCoordinates();
        for (Coordinate coordinate : coordinates) {
            double lon = DistanceUtils.toRadians(coordinate.x);
            double lat = DistanceUtils.toRadians(coordinate.y);

            Point2D.Double xy = new Point2D.Double();
            p.project(lon, lat, xy);

            coordinateProject.add(new Coordinate(xy.x, xy.y));

            System.out.println(xy);

            Point2D.Double xy_inverse = new Point2D.Double();
            p.projectInverse(xy.x, xy.y, xy_inverse);
            System.out.println(xy_inverse);

            System.out.println(DistanceUtils.toDegrees(xy_inverse.x) + "  :::: " + DistanceUtils.toRadians(xy_inverse.y));

            System.out.printf("22");

        }

        LineString lineStringProject = GeoTool.getGeoFactory().createLineString(coordinateProject.toCoordinateArray());

        Geometry geometryBuffer = BufferTool.bufferCapFat(lineStringProject, 5);
        System.out.println(geometryBuffer.toText());

        Coordinate[] coordinatesBuffer = geometryBuffer.getCoordinates();
        CoordinateList coordinatesBufferReverList = new CoordinateList();
        for (Coordinate coordinate : coordinatesBuffer) {
            Point2D.Double xy_inverse = new Point2D.Double();
            p.projectInverse(coordinate.x, coordinate.y, xy_inverse);

            double x = DistanceUtils.toDegrees(xy_inverse.x);
            double y = DistanceUtils.toDegrees(xy_inverse.y);

            coordinatesBufferReverList.add(new Coordinate(x,y));
        }

        LineString bufferResult = GeoTool.getGeoFactory().createLineString(coordinatesBufferReverList.toCoordinateArray());
        System.out.println(bufferResult.toText());


    }
}
