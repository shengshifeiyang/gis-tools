package com.dukk.gis.turf;

import com.dukk.gis.tools.DistanceTool;
import com.dukk.gis.tools.GeoJsonTool;
import com.dukk.gis.tools.WktTool;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.gson.GeometryGeoJson;
import com.mapbox.turf.TurfConstants;
import com.mapbox.turf.TurfMeasurement;
import org.junit.Test;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.geojson.GeoJsonWriter;

public class TurfTest {

    /**
     *
     * @throws ParseException
     */
    @Test
    public void test01() throws ParseException {
        Geometry line = WktTool.wktToGeo("LINESTRING (116.7262962 35.51535525, 116.7263001 35.51535535, 116.72630445 35.5153553, 116.726309 35.515355, 116.726314 35.515355, 116.7263176 35.5153549, 116.7263228 35.5153545, 116.72632685 35.5153545, 116.7263309 35.5153546)");


        GeoJsonWriter  geoJsonWriter = new GeoJsonWriter();
        String jsonString = geoJsonWriter.write(line);

        com.mapbox.geojson.Geometry geometry = GeometryGeoJson.fromJson(jsonString);

        double miles = TurfMeasurement.length((LineString) geometry, TurfConstants.UNIT_METERS);

        System.out.println(miles);


    }
}
