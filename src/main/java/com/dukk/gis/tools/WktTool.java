package com.dukk.gis.tools;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.io.WKTWriter;

/**
 * wkt操作工具类
 */
public class WktTool {

    static WKTReader wktReader = new WKTReader();

    static WKTWriter wktWriter2D = new WKTWriter();

    static WKTWriter wktWriter3D = new WKTWriter(3);

    public static Geometry wktToGeo(String wkt) throws ParseException {
        return wktReader.read(wkt);
    }

    public static String geoToWkt2D(Geometry geometry){
        return wktWriter2D.write(geometry);
    }

    public static String geoToWkt3D(Geometry geometry){
        return wktWriter3D.write(geometry);
    }
}
