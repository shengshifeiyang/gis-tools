package com.dukk.gis.tools;

import org.locationtech.jts.geom.Coordinate;

public class PointLocation {

    private int index;

    private Coordinate coordinate;

    private double segmentFraction;

    public double getSegmentFraction() {
        return segmentFraction;
    }

    public void setSegmentFraction(double segmentFraction) {
        this.segmentFraction = segmentFraction;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }
}
