package com.dukk.gis.tools;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.geojson.GeoJsonReader;
import org.locationtech.jts.io.geojson.GeoJsonWriter;

import java.io.IOException;

/**
 * geojson处理
 * @author 小豆子
 * @version 2023-03-29
 */
public class GeoJsonTool {


    public static Geometry read(String geoJson) throws ParseException {
        GeoJsonReader geoJsonReader = new GeoJsonReader();
        return geoJsonReader.read(geoJson);
    }

    public static String toJSONString(Geometry geometry){
        GeoJsonWriter geoJsonWriter = new GeoJsonWriter();
        return geoJsonWriter.write(geometry);
    }

}
