package com.dukk.gis.tools;


import org.locationtech.jts.algorithm.Angle;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;

/**
 * 夹角工具类
 */
public class AngleTool {


    /**
     * 获取顺时针夹角
     * @param p0
     * @param p1 po,p2的交点坐标
     * @param p2
     * @return
     */
    public static double getCwAngle(Coordinate p0, Coordinate p1, Coordinate p2){

        return 360-getCCwAngle(p0,p1,p2);

    }

    /**
     * 获取 逆时针夹角
     * @param p0
     * @param p1 po,p2的交点坐标
     * @param p2
     * @return
     */
    public static double getCCwAngle(Coordinate p0, Coordinate p1, Coordinate p2){
        double v = Angle.angleBetweenOriented(p0, p1, p2);
        double v1 = v * 180 / Math.PI;

        if (v1 < 0) {
            v1 += 360;
        }

        return v1;

    }

    /**
     * 获取逆时针夹角
     * lineString0的终点与lineString1的起点相交
     * @param lineString0 线
     * @param lineString1 线
     * @return
     */
    public static double getCCwAngle(Geometry lineString0, Geometry lineString1){
        Coordinate[] coordinates01 = lineString0.getCoordinates();
        Coordinate[] coordinates02 = lineString1.getCoordinates();

        Coordinate p0 = coordinates01[coordinates01.length-2];
        Coordinate p1 = coordinates01[coordinates01.length-1];
        Coordinate p2 = coordinates02[1];

        return getCCwAngle(p2,p1,p0);

    }

    /**
     * 获取顺时针夹角
     * lineString0的终点与lineString1的起点相交
     * @param lineString0 线
     * @param lineString1 线
     * @return
     */
    public static double getCwAngle(Geometry lineString0, Geometry lineString1){
        Coordinate[] coordinates01 = lineString0.getCoordinates();
        Coordinate[] coordinates02 = lineString1.getCoordinates();

        Coordinate p0 = coordinates01[coordinates01.length-2];
        Coordinate p1 = coordinates01[coordinates01.length-1];
        Coordinate p2 = coordinates02[1];

        return getCwAngle(p2,p1,p0);

    }

    /**
     * 判断点coordinate在线lineString的左边还是右边
     * @param lineString
     * @param coordinate
     * @return 左(在线上):ture 右:false
     */
    public static boolean isLeft(Geometry lineString, Coordinate coordinate){

        Coordinate[] coordinates = lineString.getCoordinates();
        Coordinate coordinateP0 = coordinates[0];
        Coordinate coordinateP1 = coordinates[1];

        //p1lines p2lineE  p3node
        //left:-0.19269756979628577
        //right:0.46404119632189633
        double angle = Angle.angleBetweenOriented(coordinateP0, coordinateP1, coordinate);
        if(angle <= 0){
            return true;
        }
        return false;

    }

    /**
     * 判断whereLineString在lineString的左侧还是右侧
     *
     * @param lineString
     * @param whereLineString 需要判断位置的线
     * @return 左(在线上):ture 右:false
     */
    public static boolean isLeft(Geometry lineString, Geometry whereLineString){

        Coordinate coordinate = whereLineString.getCoordinates()[0];
        return isLeft(lineString,coordinate);

    }


}
