package com.dukk.gis.tools.extensions.shadow;

import java.util.HashSet;
import java.util.Set;

/**
 * 形状点标注实体类
 */
public class CoordinateLabel {

    private int index; //位置，位于geometry的索引位置

    private Set<Integer> partTypeSet = new HashSet<>(); //类型

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


    public Set<Integer> getPartTypeSet() {
        return partTypeSet;
    }

    public void setPartTypeSet(Set<Integer> partTypeSet) {
        this.partTypeSet = partTypeSet;
    }

    public void addPartTypeSet(Integer partType) {
        this.partTypeSet.add(partType);
    }


}
