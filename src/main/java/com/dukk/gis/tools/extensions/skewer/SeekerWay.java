package com.dukk.gis.tools.extensions.skewer;

/**
 * 寻找方式
 */
public enum SeekerWay {

    POINT_START,
    POINT_END,
    POINT_DOUBLE
}
