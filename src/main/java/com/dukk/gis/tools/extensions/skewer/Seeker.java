package com.dukk.gis.tools.extensions.skewer;

import java.util.LinkedList;
import java.util.List;

/**
 * 寻路器 使用者继承此类,实现使用方法
 * @param <T>
 */
public interface Seeker<T> {

     /**
      *
      * @param t
      * @param dataList
      * @param message 信息 (pid或者点几何信息等)
      * @return
      * @throws Exception
      */
     List<Node<T>> findNode(T t, List<T> dataList, Object message) throws Exception;


     boolean stopCondition(LinkedList<Node<T>> parentLinkedList, Node<T> currentNode);


}
