package com.dukk.gis.tools.extensions.openlocationcode;

import com.google.openlocationcode.OpenLocationCode;

public class LocationCodeTool {

    public static void main(String[] args) {

        double x = 116.2613408;
        double y = 39.9077535;

        String code = OpenLocationCode.encode(y, x);
        System.out.println(code);

        OpenLocationCode.CodeArea codeArea = OpenLocationCode.decode(code);
        System.out.println(codeArea.getEastLongitude());//e
        System.out.println(codeArea.getWestLongitude());//w
        System.out.println(codeArea.getNorthLatitude());//e
        System.out.println(codeArea.getSouthLatitude());//e







    }
}
