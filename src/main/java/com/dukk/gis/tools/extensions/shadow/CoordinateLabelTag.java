package com.dukk.gis.tools.extensions.shadow;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 形状点标记计算
 */
public class CoordinateLabelTag {

    Geometry linkGeo = null;

    List<LineInput> lineInputList;

    Map<Coordinate, CoordinateLabel> coordinateLabelMap = new LinkedHashMap<>(); //coordinate位置索引

    public CoordinateLabelTag(List<LineInput> lineInputList, Geometry linkGeo) {
        this.linkGeo = linkGeo;
        this.lineInputList = lineInputList;

        setCoordinateLabelMap();
    }


    private void setCoordinateLabelMap(){
        Coordinate coordinate[] = this.linkGeo.getCoordinates();
        for(int i=0; i<coordinate.length;i++){
            CoordinateLabel coordinateLabel = new CoordinateLabel();
            coordinateLabel.setIndex(i);
            coordinateLabel.addPartTypeSet(PartType.EMPTY);

            this.coordinateLabelMap.put(coordinate[i], coordinateLabel);
        }
    }

    public Map<Coordinate, CoordinateLabel> doTag(){

        for(LineInput lineInput : lineInputList){
            Geometry lineString = lineInput.getLineString();
            Integer partType = lineInput.getPartType();

            Coordinate[] coordinates = lineString.getCoordinates();
            for(Coordinate coordinate : coordinates){
                CoordinateLabel coordinateLabel = this.coordinateLabelMap.get(coordinate);
                if(null != coordinateLabel){
                    coordinateLabel.addPartTypeSet(partType);
                }
            }
        }

        return this.coordinateLabelMap;
    }

}
