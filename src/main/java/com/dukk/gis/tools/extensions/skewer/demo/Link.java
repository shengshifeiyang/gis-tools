package com.dukk.gis.tools.extensions.skewer.demo;

import org.locationtech.jts.geom.Geometry;

public class Link {

    public Geometry lineString;

    public long startNodeId;

    public long endNodeId;

    public long id;

    public Geometry getLineString() {
        return lineString;
    }

    public void setLineString(Geometry lineString) {
        this.lineString = lineString;
    }

    public long getStartNodeId() {
        return startNodeId;
    }

    public void setStartNodeId(long startNodeId) {
        this.startNodeId = startNodeId;
    }

    public long getEndNodeId() {
        return endNodeId;
    }

    public void setEndNodeId(long endNodeId) {
        this.endNodeId = endNodeId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
