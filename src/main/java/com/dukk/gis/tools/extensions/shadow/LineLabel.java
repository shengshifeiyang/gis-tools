package com.dukk.gis.tools.extensions.shadow;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;

/**
 * 线段记录
 */
public class LineLabel {

    private Geometry lineString;

    private Integer partType;



    public Geometry getLineString() {
        return lineString;
    }

    public void setLineString(Geometry lineString) {
        this.lineString = lineString;
    }


    public Coordinate getFirstCoordinate(){
        return this.getLineString().getCoordinates()[0];
    }

    public Coordinate getLastCoordinate(){
        return this.getLineString().getCoordinates()[this.getLineString().getCoordinates().length-1];
    }

    public Integer getPartType() {
        return partType;
    }

    public void setPartType(Integer partType) {
        this.partType = partType;
    }


}
