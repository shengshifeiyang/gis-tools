package com.dukk.gis.tools.extensions.shadow;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 调用说明
 */
public class MainTest {

    public static void main(String[] args) throws ParseException {

        WKTReader wktReader = new WKTReader();
        Geometry linkGeo = wktReader.read("LINESTRING (106.4801065 35.5725945, 106.480132 35.572595, 106.4801725 35.572596, 106.4802005 35.572599, 106.4802215 35.572599, 106.4802455 35.572598, 106.4802685 35.572596, 106.4802835 35.5725955, 106.480302 35.5725945, 106.4803325 35.572594, 106.4803415 35.572595, 106.480379 35.5725975, 106.480415 35.572599, 106.480426 35.5725985, 106.480445 35.572598, 106.480465 35.5725965, 106.480484 35.5725955, 106.480504 35.5725965, 106.480523 35.5725955, 106.4805605 35.572595, 106.4805815 35.572594)");

        List<LineInput> lineInputList = new ArrayList<>();

        LineInput lineInput = new LineInput();
        lineInput.setLineString(wktReader.read("LINESTRING (106.48015233333334 35.57260933333333, 106.480171 35.572612, 106.48018266666666 35.572611333333334, 106.48018466666667 35.572611333333334, 106.480196 35.572611333333334, 106.48019733333334 35.572611333333334, 106.48019866666667 35.572611333333334, 106.48020033333333 35.572611333333334, 106.480214 35.57261066666667, 106.48021533333333 35.57261066666667, 106.480228 35.57261066666667, 106.480229 35.57261, 106.48024133333334 35.57261, 106.48025533333333 35.57261)"));
        lineInput.setPartType(1);
        lineInput.setNeedShadow(true);
        lineInputList.add(lineInput);



        LineInput lineInput02 = new LineInput();
        lineInput02.setLineString(wktReader.read("LINESTRING (106.48037233333334 35.572609, 106.48039466666667 35.572612, 106.48039566666667 35.57261233333333, 106.48041 35.572612, 106.480411 35.572611333333334, 106.48041233333333 35.572611333333334, 106.48043166666666 35.572611333333334, 106.48044766666666 35.57261066666667, 106.48044866666666 35.57261, 106.48044966666667 35.572609666666665, 106.48047 35.57260866666667)"));
        lineInput02.setPartType(2);
        lineInput02.setNeedShadow(true);
        lineInputList.add(lineInput02);



        LineLabelFactory lineLabelFactory = new LineLabelFactory(lineInputList, linkGeo);
        List<LineLabel> lineLabelList = lineLabelFactory.getLineLabel(partTypeSet->{
            if(partTypeSet.contains(1)){
                return 1;
            }else if(partTypeSet.contains(2)){
                return 2;
            }else {
                return PartType.EMPTY;
            }
        });

        for (LineLabel lineLabel : lineLabelList){
            System.out.println("TYPE:"+lineLabel.getPartType()+"; geo: " + lineLabel.getLineString().toText() + "\r\n");
        }
    }
}
