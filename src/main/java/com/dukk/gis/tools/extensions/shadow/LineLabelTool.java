package com.dukk.gis.tools.extensions.shadow;

import cn.hutool.core.util.NumberUtil;
import com.dukk.gis.tools.LineTool;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import java.util.LinkedList;
import java.util.List;

/**
 * 工具类
 */
public class LineLabelTool {

    /**
     * 通过一根线上面的线段信息,通过等比例关系去截取另一个线上面的信息
     * @param targetGeometry 目标几何信息
     * @param sourceGeometry 原几何信息
     * @param lineLabelList sourceGeometry原几何截取的分组信息
     * @return targetGeometry几何截取的分组信息
     */
    public static List<LineLabel> lineLabelToGeometry(Geometry targetGeometry, Geometry sourceGeometry, List<LineLabel> lineLabelList){
        List<LineLabel> lineLabels = new LinkedList<>();

        double sourceLength = sourceGeometry.getLength();
        double totalLength = 0D;

        double targetLength = targetGeometry.getLength();

        Coordinate beforeCoor = null;

        for (int i=0; i< lineLabelList.size(); i++){
            LineLabel lineLabel = lineLabelList.get(i);

            Geometry lineString = lineLabel.getLineString();
            int partType = lineLabel.getPartType();

            double currentLength = lineString.getLength();

            totalLength = totalLength + currentLength;

            double ratio = NumberUtil.div(totalLength, sourceLength);

            double targetRatioLength = NumberUtil.mul(targetLength, ratio);

            Coordinate currentCoordinate = LineTool.ST_InterpolatePoint(targetGeometry, targetRatioLength);

            if(i == 0){
                beforeCoor = lineString.getCoordinates()[0];
            }

            Geometry extractLine = LineTool.getExtractLine(targetGeometry, beforeCoor, currentCoordinate);

            LineLabel currentLineLabel = new LineLabel();
            currentLineLabel.setLineString(extractLine);
            currentLineLabel.setPartType(partType);

            lineLabels.add(currentLineLabel);

            beforeCoor = currentCoordinate;

        }

        return lineLabels;
    }
}
