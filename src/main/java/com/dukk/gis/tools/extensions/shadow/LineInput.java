package com.dukk.gis.tools.extensions.shadow;

import org.locationtech.jts.geom.Geometry;

/**
 * 线段组
 */
public class LineInput {

    private Geometry lineString;

    private Integer partType;

    private boolean isNeedShadow = false;

    public Geometry getLineString() {
        return lineString;
    }

    public void setLineString(Geometry lineString) {
        this.lineString = lineString;
    }

    public Integer getPartType() {
        return partType;
    }

    public void setPartType(Integer partType) {
        this.partType = partType;
    }

    public boolean isNeedShadow() {
        return isNeedShadow;
    }

    public void setNeedShadow(boolean needShadow) {
        isNeedShadow = needShadow;
    }
}
