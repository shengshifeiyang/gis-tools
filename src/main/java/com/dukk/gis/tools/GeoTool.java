package com.dukk.gis.tools;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;

public class GeoTool {

   static GeometryFactory GEOMETRY_FACTORY = new GeometryFactory();


   public static GeometryFactory getGeoFactory(){
       return GEOMETRY_FACTORY;
   }

    public static Coordinate createCoordinate(double x, double y){
        Coordinate coordinate = new Coordinate();
        coordinate.x = x;
        coordinate.y = y;

        return coordinate;
    }

    public static Geometry createPolygon(Coordinate[] coordinates){
        return GEOMETRY_FACTORY.createPolygon(coordinates);
    }


}
